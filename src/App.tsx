import React from 'react'; 
import './App.css';
import {useDropzone} from 'react-dropzone'
import Papa from 'papaparse'

import Converter from './Converter';

function App() {

  const [fileType, setFileType] = React.useState<string | null>(null)
  const [dataset, setDataset] = React.useState(null)

  const onCsvParseComplete = ({ data }: any) => {
     setDataset(data)
  }
  
  const onDrop = (files: File[]) => {
    if(["text/plain", "text/xml", "application/json"].includes(files[0].type)){ 
      setFileType(files[0].type)
      Papa.parse(files[0], {
        complete: onCsvParseComplete,
      })
    } 
    else{
      alert("Formato no permitido")
    }
  }

  const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop})
  return (
    <div className="App">
      <header className="App-header"> 
        <p>Seleccione un archivo.</p> 
      </header>
      <div {...getRootProps()} className="App-Dropzone">
        <input {...getInputProps()} />
        {
          isDragActive ?
            <p>Suelte su archivo ...</p> :
            <p>Arrastre o seleccione un archivo en esta zona</p>
        }
      </div>
      {typeof fileType === "string" && 
        <Converter fileType={fileType} dataset={dataset} />
      }
    </div>
  );
}

export default App;
