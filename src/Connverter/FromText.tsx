import React from 'react'; 
import '../App.css'; 

interface IFromTextProps { 
  dataset: any | null
}

function FromText({dataset}: IFromTextProps) {
  
  return (
    <div className="App"> 
      <div className="sections-wrapper">
        <div className="section-json">
          <h3 className="section-title">JSON Data</h3> <br />
          <div className="json-string">
            [
            {
              dataset!== null && dataset.map((data: any, index: number) => {
                if(dataset[0][index]!=="document"){
                  return(
                    <span key={index} className="json-internal-node"> 
                      {"{"}
                        <br /> 
                        {data.map((inf: any, j: number) => { 
                          return (
                            <span className="json-data-node" key={j}>
                              <span className="dataset-title">{ dataset[0][j] }:</span>
                              <span className="dataset-value">{ inf }</span>,
                            </span>
                          ) 
                        })} 
                        
                      {"},"} 
                    </span>
                  )
                }
              })
            }
            ]
            </div>
        </div>
        <div className="section-xml">
          <h3 className="section-title">XML Data</h3>
          <div className="xml-string">
          {"<clientes>"}
            {
              dataset!== null && dataset.map((data: any, index: number) => {
                if(dataset[0][index]!=="document"){
                  return(
                    <span key={index} className="xml-internal-node"> 
                      <br />
                      {"<cliente>"}
                        <br /> 
                        {data.map((inf: any, j: number) => { 
                          return (
                            <span className="xml-data-node" key={j}>
                              <span className="dataset-json-title">{ `<${dataset[0][j]}>` }</span>
                              <span className="dataset-json-value">{ inf }</span> 
                              <span className="dataset-json-title">{ `</${dataset[0][j]}>` }</span>
                            </span>
                          ) 
                        })}
                      {"</cliente>"} 
                    </span>
                  )
                }
              })
            }
            {"</clientes>"}
            </div>
        </div>
      </div>
    </div>
  );
}

export default FromText;
