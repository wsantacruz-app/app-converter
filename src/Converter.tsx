import React from 'react'; 
import './App.css'; 
import FromText from './Connverter/FromText';

interface IConverterProps {
  fileType: string
  dataset: any | null
}

function Converter({fileType, dataset}: IConverterProps) {

  const dataConverted = () => {
    if(fileType==="text/plain")
    {
      return(
        <FromText dataset={dataset}/>
      )
    }
    else {
      return <div>Other format</div>
    }
  }
 
 
  return (
    <div className="App"> 
      <p>Formato del Archivo Cargado: <b>{fileType}</b> </p>
      {dataConverted()}
    </div>
  );
}

export default Converter;
